
// $('body').addClass('sidebar-collapse')
$('body').removeClass('sidebar-mini')
$('body').addClass('layout-top-nav')
$('.content-header').hide()

$('a.nav-link.d-none.d-lg-block').click(function () {
	// $('body').toggleClass('sidebar-collapse')
	$('body').toggleClass('sidebar-mini')
	$('body').toggleClass('layout-top-nav')
})

$('.scroll-page').on('click', function (e) {
	let tujuan = $('.scroll-page').attr('href')

	let elemenTujuan = $(tujuan)

	$('html, body').animate({
		scrollTop : elemenTujuan.offset().top -39
	}, 'linear');
	e.preventDefault()
})

let table;

$(function() {
	$('.select2').select2()
	$('.tabel-bus').DataTable();

	$('.datepicker').datepicker({
        format: 'dd-mm-yyyy'
    });

	table = $('.tabel-penjualan').DataTable({
		'dom' : 'Brt',
		'bSort' : false,
		'serverSide' : true,
		'autoWidth' : false,
		'ajax' : {
			'url' : '{{ route('transaksi.data', $idbooking) }}',
			'type' : 'GET'
		}
		
	}).on('draw.dt', function() {
		loadForm($('#diskon').val());
	})
})

$('.form-bus, .form-keranjang').on('submit', function() {
	return false;
})

$('#kode').change(function() {
	addItem();
})

$('#member').change(function() {
	selectMember($(this).val())
})

// Jalankan fungsi loadForm() ketika diterima diubah
$('#diterima').change(function() {
	if ($(this).val() == '') $(this).val(0).select()
	loadForm($('#diskon').val(), $(this).val())
}).focus(function() {
	$(this).select()
})

// menyimpan form transaksi
$('.simpan').click(function() {
	$('.form-penjualan').submit();
})

function addItem() {
	$.ajax({
		url : '{{ route('transaksi.store') }}',
		type : 'POST',
		data : $('.form-bus').serialize(),
		success : function(data) {
			$('#kode').val('').focus();
			table.ajax.reload()
			// table.ajax.reload(function(){
			// 	loadForm($('#diskon').val())
			// })
		}, 
		error : function() {
			alert('Tidak dapat menyimpan data!');
		}
	})
}

function showBus() {
	$('#modal-bus').modal('show');
}

function showMember() {
	$('#modal-member').modal('show');
}

function selectItem(id, kode) {
	$('#id').val(id);
	$('#kode').val(kode);
	$('#modal-bus').modal('hide');
	addItem();
}

function changeCount(id, stok) {
	// if($('#count').val() > stok) {
	// 	alert('Stok tersisa '+ stok);
	// 	$('#count').val('')
	// }

	$.ajax({
		url  : 'transaksi/'+ id,
		type : 'POST',
		data : $('.form-keranjang').serialize(),
		success : function(data) {
			table.ajax.reload(function() {
				loadForm($('#diskon').val());
			})
		}, 
		error : function() {
			alert('Tidak dapat menyimpan data!');
		}
	})
}

function selectMember(id, kode) {
	$('#modal-member').modal('hide')
	
	$('#id_member').val(id)
	$('#member').val(kode)
	loadForm($('#diskon')).val()
	$('#diterima').val(0).focus().select()
}

function deleteItem(id) {
	if(confirm('Apakah yakin data akan dihapus?')) {
		$.ajax({
			url : 'transaksi/'+ id,
			type : 'POST',
			data : {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
			success :  function(data) {
				table.ajax.reload(function() {
					loadForm($('#diskon'));
				})
			},
			error : function(data) {
				alert('Tidak dapat menghapus data!');
			}
		})
	}
}

function loadForm(diskon = 0, diterima = 0) {
	$('#total').val($('.total').text());
	$('#totalitem').val($('.totalitem').text());
	
	if(diskon) diskon = diskon;
	else diskon = 0;

	$.ajax({
		url : 'transaksi/loadform/' + diskon + '/' + $('#total').val() + '/' + diterima,
		type : 'GET',
		dataType : 'JSON',
		success : function(data) {
			$('#totalrp').val('Rp. '+ data.totalrp);
			$('#bayarrp').val('Rp. '+ data.bayarrp);
			$('#bayar').val(data.bayar);
			$('#tampil-bayar').text('Bayar: Rp. '+ data.bayarrp);
			$('#tampil-terbilang').text(data.terbilang);

			$('#kembali').val('Rp. '+ data.kembalirp);
			if($('#diterima').val() != 0) {
				$('#tampil-bayar').html('<small>Kembali:</small> Rp. '+ data.kembalirp)
				$('#tampil-terbilang').text(data.kembaliterbilang);
			}
		},
		error : function(data) {
			alert('Tidak dapat menampilkan data!');
		}
	})
}

// $(function() {
// 	$('.cari-bus').on('submit', function (e) {
// 		e.preventDefault()

// 		let tujuan = $('#tujuan').val()
// 		let pergi  = $('#pergi').val()
// 		let pulang = $('#pulang').val()

// 		$.ajax({
// 			url: 'transaksi/getdatabus/'+ tujuan + '/' + pergi + '/' + pulang,
// 			type: 'get',
// 			data : $('.cari-bus').serialize(),
// 			success : function(data) {
					
// 			}, 
// 			error : function() {
// 				alert('Tidak dapat menyimpan data!');
// 			}
// 		})

// 	})
// })
