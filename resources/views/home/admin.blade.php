@extends('layouts.master')

@section('title', 'Dashboard')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active">Dashboard</li>
@endsection

@push('css')
    <style>
        .fc-title {
            color: #fff;
        }
    </style>
@endpush

@section('main-content')
<div class="row">
    <div class="col-6 col-md-3">
        <div class="small-box bg-primary">
            <div class="inner">
                <h3>10</h3>
                <p>Total Pesanan</p>
            </div>
            <div class="icon">
                <i class="ion ion-bag"></i>
            </div>
            <a href="{{ url('/orders') }}" class="small-box-footer">Lihat <i class="fas fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-6 col-md-3">
        <div class="small-box bg-success">
            <div class="inner">
                <h3>10</h3>
                <p>Total Customer</p>
            </div>
            <div class="icon">
                <i class="ion ion-person-add"></i>
            </div>
            <a href="{{ url('/customers') }}" class="small-box-footer">Lihat <i class="fas fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-6 col-md-3">
        <div class="small-box bg-purple">
            <div class="inner">
                <h3>10</h3>
                <p>Total Karyawan</p>
            </div>
            <div class="icon">
                <i class="ion ion-person-add"></i>
            </div>
            <a href="{{ url('/employees') }}" class="small-box-footer">Lihat <i class="fas fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-6 col-md-3">
        <div class="small-box bg-danger">
            <div class="inner">
                <h3>10</h3>
                <p>Total Kendaraan</p>
            </div>
            <div class="icon">
                <i class="fas fa-bus"></i>
            </div>
            <a href="{{ url('/buses') }}" class="small-box-footer">Lihat <i class="fas fa-arrow-circle-right"></i></a>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-md-3">
        <div class="row">
            <div class="col-6 col-md-12">
                <div class="small-box bg-gray-dark">
                    <div class="inner">
                        <h3>20</h3>
                        <p>Order Hari Ini</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="{{ url('/orders') }}?date={{ date('Y-m-d') }}" class="small-box-footer">Lihat <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-6 col-md-12">
                <div class="small-box bg-indigo">
                    <div class="inner">
                        <h3>20</h3>
                        <p>SPJ Harus Dicetak</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="{{ url('/orders') }}" class="small-box-footer">Lihat <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-md-9">
        <div class="card card-primary card-outline">
            <div class="card-header border-bottom-0 p-0">
                <p class="text-center card-title">Grafik Pendapatan </p>
                <p class="text-center">{{ tanggal_indonesia(date('Y-m-d')) }} s/d {{ tanggal_indonesia(date('Y-m-d')) }}</p>
                
            </div>
            <div class="card-body">
                <div class="chart">
                    <!-- Sales Chart Canvas -->
                    <canvas id="salesChart" height="185" style="height: 185px;"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="sticky-top mb-3">
            <div class="card d-none d-lg-none">
                <div class="card-body">
                    <div id="external-events">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card">
            <div class="card-body p-0">
                <div id="calendar"></div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-notif">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content rounded-0">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <p class="card-title">15 (SPJ) harus dicetak.</p>
                <p>Pastikan anda mencetaknya hari ini.</p>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script src="{{ asset('public/adminlte/plugins/jquery-mousewheel/jquery.mousewheel.js') }}"></script>
<script src="{{ asset('public/adminlte/plugins/raphael/raphael.min.js') }}"></script>
<script src="{{ asset('public/adminlte/plugins/jquery-mapael/jquery.mapael.min.js') }}"></script>
<script src="{{ asset('public/adminlte/plugins/jquery-mapael/maps/world_countries.min.js') }}"></script>
<script src="{{ asset('public/adminlte/plugins/chart.js/Chart.min.js') }}"></script>
<script src="{{ asset('public/adminlte/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('public/adminlte/plugins/fullcalendar/main.min.js') }}"></script>
<script src="{{ asset('public/adminlte/plugins/fullcalendar-daygrid/main.min.js') }}"></script>
<script src="{{ asset('public/adminlte/plugins/fullcalendar-timegrid/main.min.js') }}"></script>
<script src="{{ asset('public/adminlte/plugins/fullcalendar-interaction/main.min.js') }}"></script>
<script src="{{ asset('public/adminlte/plugins/fullcalendar-bootstrap/main.min.js') }}"></script>
<script>
    
    $('.table').DataTable({
        'paginate': false,
        'searching': false,
        'bInfo': false,
        "language": {
            "emptyTable": "Ups data masih kosong!.",
            "zeroRecords": "Ups data tidak ditemukan!."
        },
    })

    $(function() {
        var salesChartCanvas = $('#salesChart').get(0).getContext("2d");
        var salesChartData = {
            labels: 'TEst H.',
            datasets: [
                {
                    label: 'Pendapatan',
                    backgroundColor: [
                        'rgba(0, 123, 255, .9)'
                        
                    ],
                    borderColor: [
                        'rgba(0, 123, 255, 1)'
                    ],
                    data: '-'
                }
            ]
        }

        var salesChartOptions = {
            maintainAspectRatio : false,
            responsive : true,
            legend: {
                display: false
            },
            scales: {
                xAxes: [{
                    gridLines : {
                        display : false,
                    }
                }],
                yAxes: [{
                    gridLines : {
                    display : false,
                    }
                }]
            }
        }

        var salesChart = new Chart(salesChartCanvas, { 
            type: 'line', 
            data: salesChartData, 
            options: salesChartOptions
        })
    })


    $(function () {
        
        calendar()

        /* initialize the external events
         -----------------------------------------------------------------*/
        function ini_events(ele) {
          ele.each(function () {

            // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
            // it doesn't need to have a start or end
            var eventObject = {
              title: $.trim($(this).text()) // use the element's text as the event title
            }

            // store the Event Object in the DOM element so we can get to it later
            $(this).data('eventObject', eventObject)

            // make the event draggable using jQuery UI
            $(this).draggable({
              zIndex        : 1070,
              revert        : true, // will cause the event to go back to its
              revertDuration: 0  //  original position after the drag
            })

          })
        }

        function calendar() {
            ini_events($('#external-events div.external-event'))

            /* initialize the calendar
             -----------------------------------------------------------------*/
            //Date for the calendar events (dummy data)
            var date = new Date()
            var d    = date.getDate(),
                m    = date.getMonth(),
                y    = date.getFullYear()

            var Calendar = FullCalendar.Calendar;
            var Draggable = FullCalendarInteraction.Draggable;

            var containerEl = document.getElementById('external-events');
            var checkbox = document.getElementById('drop-remove');
            var calendarEl = document.getElementById('calendar');

            // initialize the external events
            // -----------------------------------------------------------------

            new Draggable(containerEl, {
              itemSelector: '.external-event',
              eventData: function(eventEl) {
                console.log(eventEl);
                return {
                  title: eventEl.innerText,
                  backgroundColor: window.getComputedStyle( eventEl ,null).getPropertyValue('background-color'),
                  borderColor: window.getComputedStyle( eventEl ,null).getPropertyValue('background-color'),
                  textColor: window.getComputedStyle( eventEl ,null).getPropertyValue('color'),
                };
              }
            });

            var calendar = new Calendar(calendarEl, {
              plugins: [ 'bootstrap', 'interaction', 'dayGrid', 'timeGrid' ],
              header    : {
                left  : 'prev,next today',
                center: 'title',
                right : 'dayGridMonth,timeGridWeek,timeGridDay'
              },
              events    : '{{ url('/load/events') }}',
              editable  : true,
              droppable : true, // this allows things to be dropped onto the calendar !!!
              drop      : function(info) {
                // is the "remove after drop" checkbox checked?
                if (checkbox.checked) {
                  // if so, remove the element from the "Draggable Events" list
                  info.draggedEl.parentNode.removeChild(info.draggedEl);
                }
              },
            });

            calendar.render();
            // $('#calendar').fullCalendar()

            /* ADDING EVENTS */
            var currColor = '#3c8dbc' //Red by default
            //Color chooser button
            var colorChooser = $('#color-chooser-btn')
            $('#color-chooser > li > a').click(function (e) {
              e.preventDefault()
              //Save color
              currColor = $(this).css('color')
              //Add color effect to button
              $('#add-new-event').css({
                'background-color': currColor,
                'border-color'    : currColor
              })
            })
            $('#add-new-event').click(function (e) {
              e.preventDefault()
              //Get value and make sure it is not null
              var val = $('#new-event').val()
              if (val.length == 0) {
                return
              }

              //Create events
              var event = $('<div />')
              event.css({
                'background-color': currColor,
                'border-color'    : currColor,
                'color'           : '#fff'
              }).addClass('external-event')
              event.html(val)
              $('#external-events').prepend(event)

              //Add draggable funtionality
              ini_events(event)

              //Remove event from text input
              $('#new-event').val('')
            })
        }
    })
</script>
@endpush