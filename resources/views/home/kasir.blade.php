@extends('layouts.master')

@section('title', 'Dashboard')

@section('bredcrumb')
	@parent
	<li>Dashboard</li>
@endsection

@section('main-content')
	<div class="row">
		<div class="col-md-12">
			<div class="card">
			    <div class="card-body text-center">
			    	<h1 class="font-weight-bold">Selamat Datang,</h1>
			    	<h4>Anda login sebagai Kasir</h4>
			    	<br><br>
			    	<a href="{{ route('transaksi.new') }}" class="btn btn-success btn-lg">Booking Sekarang</a>
			    	<br><br><br>
			    </div>
			</div>
		</div>
	</div>
@endsection