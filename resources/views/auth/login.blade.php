@extends('layouts.auth')

@section('title')
    Login - Administrator
@endsection

@section('content')
<div class="login-box">
    <div class="login-logo">
        <p><b>Login</b> Administrator</p>
    </div>
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Sign in to start your session</p>
            <form action="" method="post">
                @csrf
                <div class="form-group has-feedback">
                    <input type="text" class="form-control 
                        {{ $errors->has('email') ? 'is-invalid' : '' }}
                        {{ $errors->has('username') ? 'is-invalid' : '' }}
                    " placeholder="Username / Email." name="identity" value="{{ old('identity') }}" required autofocus>
                    
                    <span class="fa fa-envelope form-control-feedback">
                        @if ($errors->has('email'))
                            {{ $errors->first('email') }}
                        @endif

                        @if ($errors->has('username'))
                            {{ $errors->first('username') }}
                        @endif
                    </span>

                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" placeholder="Password" name="password" value="{{ old('password') }}">
                    <span class="fa fa-lock form-control-feedback">
                    @if ($errors->has('password'))
                        {{ $errors->first('password') }}
                    @endif
                    </span>
                </div>
                <div class="row">
                    <div class="col-8">
                        <div class="icheck-primary">
                            <input type="checkbox" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label for="remember">
                                Remember Me
                            </label>
                        </div>
                    </div>
                    
                    <div class="col-4">
                        <button class="btn btn-primary btn-block">Sign In</button>
                    </div>
                </div>
            </form>
            <div class="social-auth-links text-center mb-3">
                <p>- OR -</p>
                <a href="#" class="btn btn-block btn-primary">
                    <i class="fab fa-facebook mr-2"></i> Sign in using Facebook
                </a>
                <a href="#" class="btn btn-block btn-danger">
                    <i class="fab fa-google-plus mr-2"></i> Sign in using Google+
                </a>
            </div>
        </div>
        <!-- /.login-card-body -->
    </div>
</div>
@endsection