<nav class="main-header navbar navbar-expand bg-primary navbar-dark">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link d-none d-lg-block" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>

            <a class="nav-link d-lg-none d-block" data-widget="pushmenu" href="#" style="margin-left: -1em;">
                <img src="{{ asset('public/adminlte/dist/img/logo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3">
                <span class="brand-text font-weight-light">BS Guvilli System</span>
            </a>
        </li>
        <li class="nav-item d-none d-lg-block" id="home">
            <a href="{{ url('/') }}" class="nav-link">Home</a>
        </li> 
    </ul>

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3 d-none d-lg-none">
        <div class="input-group input-group-sm">
            <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
            <div class="input-group-append">
                <button class="btn btn-navbar" type="submit">
                <i class="fas fa-search"></i>
                </button>
            </div>
        </div>
    </form>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="far fa-bell"></i>
                <span class="badge badge-danger navbar-badge">{{ $print_spj_today->count() }}</span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <span class="dropdown-item dropdown-header">{{ $print_spj_today->count() }} (SPJ) Harus Segera Dicetak.</span>
                <div class="dropdown-divider"></div>
                @foreach ($print_spj_today as $spj)
                    <a target="_blank" href="{{ url('/orders') }}/{{ $spj->booking_id }}/cetak/spj/{{ $spj->bus_id }}?{{ rand() }}" class="dropdown-item">
                        <i class="ion ion-bag mr-2"></i> SPJ untuk Bus {{ $spj->bus['no_polisi'] }}
                    </a>
                    <div class="dropdown-divider"></div>

                @endforeach
                <a href="{{ url('/orders') }}?date={{ date('Y-m-d') }}" class="dropdown-item dropdown-footer">Lihat Semua Pemberitahuan.</a>
            </div>
        </li>
        <a href="{{ route('logout') }}" class="nav-item nav-link" onclick="event.preventDefault(); document.getElementById('logout-form').submit()"><i class="fas fa-sign-out-alt"></i> Sign Out</a>
    </ul>

    <form action="{{ route('logout') }}" method="post" class="d-none d-lg-none" id="logout-form">
        @csrf
    </form>
</nav>