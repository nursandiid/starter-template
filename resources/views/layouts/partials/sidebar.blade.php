<aside class="main-sidebar sidebar-light-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link navbar-primary text-white">
        <img src="{{ asset('public/adminlte/dist/img/logo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="height: 32px; opacity: .8px">
        <span class="brand-text font-weight-light">BS Guvilli System</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ Storage::disk('public')->url('uploads/users/') }}/{{ Auth::user()->foto }}" class="img-circle elevation-2" alt="User Image" id="img-profile">
            </div>
            <div class="info">
                <a href="{{ url('/users/profile') }}" class="d-block name-profile">{{ Auth::user()->name }}</a>
            </div>
        </div>
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{ url('/home') }}" class="nav-link">
                        <i class="nav-icon fas fa-home"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                
                @if(Auth::user()->level == 1)
                <li class="nav-header">MASTER MENU</li>
                <li class="nav-item has-treeview menu-master">
                    <a href="{{ url('/employees') }}" class="nav-link">
                        <i class="nav-icon fas fa-th-large"></i>
                        <p>
                            Master Data
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ url('/employees') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Karyawan</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/expertises') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Keahlian</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/categories') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Kategori Kendaraan</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/buses') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Kendaraan</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/seats') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Data Seat</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/destinations') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Data Tujuan</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-header">TRANSACTION MENU</li>
                <li class="nav-item">
                    <a href="{{ url('/transaksi') }}" class="nav-link">
                        <i class="nav-icon fas fa-cart-arrow-down"></i>
                        <p>Booking Bus</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('/orders') }}" class="nav-link">
                        <i class="nav-icon fas fa-donate"></i>
                        <p>Pembayaran</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('/transaksi/show-bus-unit') }}" class="nav-link">
                        <i class="nav-icon fas fa-bus"></i>
                        <p>Penentuan Unit Bus</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('/schedules') }}" class="nav-link">
                        <i class="nav-icon far fa-calendar-check"></i>
                        <p>Penjadwalan</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('/make-spj') }}" class="nav-link">
                        <i class="nav-icon fas fa-envelope-open-text"></i>
                        <p>Pembuatan Surat Jalan</p>
                    </a>
                </li>

                <li class="nav-header">REPORT</li>
                <li class="nav-item has-treeview menu-master" id="laporan">
                    <a href="{{ url('/reports/schedules') }}" class="nav-link">
                        <i class="nav-icon fas fa-file-pdf"></i>
                        <p>
                            Laporan
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ url('/reports/schedules') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Jadwal Bus</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/reports/orders') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Data Order</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/customers') }}" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>Customer</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/expenses') }}" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>Pengeluaran</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/reports/incomes') }}" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>Pendapatan</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-header">SYSTEM</li>
                <li class="nav-item">
                    <a href="{{ url('/settings') }}" class="nav-link">
                        <i class="nav-icon fas fa-cogs"></i>
                        <p>Setting</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('/users') }}" class="nav-link">
                        <i class="nav-icon fas fa-users"></i>
                        <p>Manajemen User</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('logout') }}" class="nav-link">
                        <i class="nav-icon fas fa-sign-out-alt"></i>
                        <p>Sign Out</p>
                    </a>
                </li>

                @elseif(Auth::user()->level == 2)
                <li class="nav-item">
                    <a href="{{ url('/transaksi') }}" class="nav-link">
                        <i class="nav-icon fas fa-cart-arrow-down"></i>
                        <p>Booking Bus</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('logout') }}" class="nav-link">
                        <i class="nav-icon fas fa-sign-out-alt"></i>
                        <p>Sign Out</p>
                    </a>
                </li>
                @endif
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>