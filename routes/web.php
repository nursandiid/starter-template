<?php

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();
Route::group(['middleware' => ['web', 'auth', 'checklevel:1']], function() {
	Route::get('/home', 'HomeController@index')->name('home');
});

Route::group(['middleware' => ['web', 'auth']], function() {
	Route::get('/home', 'HomeController@index')->name('home');

});